<?php

return [
    'url' => env('STRAPI_URL'),
    'cacheTime' => intval(env('STRAPI_CACHE_TIME', 3600)),
    'token' => env('STRAPI_TOKEN'),
];
