## Langkah-langkah Instalasi

1. **Install Dependencies:**

   Jalankan perintah berikut untuk menginstall semua dependensi yang dibutuhkan:
   
   ```bash
   composer install

   ```bash
   cp .env.example .env

    ```bash
   php artisan key:generate

    ```bash
   composer require dbfx/laravel-strapi

   Lalu tambahkan settingan strapi pada .env
     ```bash
   STRAPI_URL=http://inka-cms.kaewae.cloud/api
    STRAPI_CACHE_TIME=3600
    STRAPI_TOKEN=424d880db7bac9339a50d22cf15f53997b69967ea679fc91653b782f04204fef45255a991027d7a192f3e8b8d30ea46b8152b2c1b197af348bdfbf49804f4ae719d886e7a22d9e7b4f104f4d412ed1aa0eecfb0b9915179d044242e00c16ada4487a643751469ff92dab9505a1eab1b31f566ced5b8c57207aabffcb10afad9c

